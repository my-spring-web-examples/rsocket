# rsocket-study

## 普通のレスポンス

```bash
curl localhost:8080/hello
```

## RSocketのリクエスト

RSocketController#requestResponse がレスポンスしている

```bash
rsc --debug --request --data "{\"origin\":\"Client\",\"interaction\":\"Request\"}" --route request-response tcp://localhost:7000/rsocket
```

## サーバーからRSocketリクエストを送信するエンドポイント

RSocketClientHandler が返している。

```bash
 curl localhost:8080/test
```