package com.example.rsocket.controller

import com.example.rsocket.model.Message
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.stereotype.Controller
import reactor.core.publisher.Mono

@Controller
class RSocketController {
    @MessageMapping("request-response")
    fun requestResponse(request: Message): Mono<Message> {
        println(request)
        // create a single Message and return it
        return Mono.just(Message(request.origin, "interaction", 1))
    }
}
