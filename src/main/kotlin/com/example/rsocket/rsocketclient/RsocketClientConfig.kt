package com.example.rsocket.rsocketclient

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.messaging.rsocket.RSocketRequester

@Configuration
class RsocketClientConfig() {
    @Bean
    fun rSocketClient(rsocketRequesterBuilder: RSocketRequester.Builder): RSocketRequester {
        return rsocketRequesterBuilder
            .tcp("localhost", 7000)
    }
}
