package com.example.rsocket.handler

import com.example.rsocket.model.Message
import org.springframework.http.MediaType
import org.springframework.messaging.rsocket.RSocketRequester
import org.springframework.messaging.rsocket.retrieveMono
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

@Component
class RSocketClientHandler(private val rSocketRequester: RSocketRequester) {
    fun test(request: ServerRequest): Mono<ServerResponse> {
        return rSocketRequester.route("request-response").data(Message("local", "test", 2)).retrieveMono<Message>()
            .flatMap { ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).bodyValue(it) }
    }
}
