package com.example.rsocket.model

import java.time.Instant

data class Message(
    val origin: String,
    val interaction: String,
    val index: Long,
    val created: Long = Instant.now().epochSecond
)
